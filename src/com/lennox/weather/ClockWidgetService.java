/*
 * Copyright (C) 2012 The CyanogenMod Project (DvTonder)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather;

import android.app.AlarmManager;
import android.app.Service;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.IBinder;
import android.provider.Settings;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.RemoteViews;

import com.lennox.weather.misc.Constants;
import com.lennox.weather.misc.Preferences;
import com.lennox.weather.misc.WidgetUtils;
import com.lennox.weather.utils.ThemeUtils;
import com.lennox.weather.weather.WeatherInfo;
import com.lennox.weather.weather.WeatherUpdateService;
import java.util.Date;
import java.util.Locale;

public class ClockWidgetService extends Service {
    private static final String TAG = "ClockWidgetService";
    private static final boolean D = Constants.DEBUG;

    public static final String ACTION_REFRESH = "com.lennox.weather.action.REFRESH_WIDGET";

    private static Typeface mRobotoSlab;

    private int[] mWidgetIds;
    private AppWidgetManager mAppWidgetManager;

    private BroadcastReceiver bReceiver;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getApplicationContext().unregisterReceiver(bReceiver);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ComponentName thisWidget = new ComponentName(this, ClockWidgetProvider.class);
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mWidgetIds = mAppWidgetManager.getAppWidgetIds(thisWidget);

        if ( mRobotoSlab == null ) {
            mRobotoSlab =
                Typeface.createFromAsset(getApplicationContext().getAssets(), "fonts/RobotoSlab.ttf");
        }

        bReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleReceivedCommand(intent);
            }
        };

        handleReceivedCommand(intent);

        IntentFilter intentFilter = new IntentFilter(Intent.ACTION_TIME_TICK);
        intentFilter.addAction(Intent.ACTION_TIMEZONE_CHANGED);
        intentFilter.addAction(Intent.ACTION_TIME_CHANGED);
        intentFilter.addAction(Intent.ACTION_SCREEN_ON);
        intentFilter.addAction(ACTION_REFRESH);
        getApplicationContext().registerReceiver(bReceiver, intentFilter);

        return START_STICKY;
    }

    private void handleReceivedCommand(Intent intent) {
        ComponentName thisWidget = new ComponentName(this, ClockWidgetProvider.class);
        mAppWidgetManager = AppWidgetManager.getInstance(this);
        mWidgetIds = mAppWidgetManager.getAppWidgetIds(thisWidget);

        if (mWidgetIds != null && mWidgetIds.length != 0) {
            refreshWidget();
        }
    }

    /**
     * Reload the widget including the Weather forecast, Alarm, and clock
     */
    private void refreshWidget() {
        // Get things ready
        RemoteViews remoteViews;

        // Update the widgets
        for (int id : mWidgetIds) {

            if (Preferences.weatherWidgetStyle(this).equals("transparent")) {
                remoteViews = new RemoteViews(getPackageName(), R.layout.appwidget_transparent);
            } else if (Preferences.weatherWidgetStyle(this).equals("panel")) {
                remoteViews = new RemoteViews(getPackageName(), R.layout.appwidget_panel);
            } else {
                remoteViews = new RemoteViews(getPackageName(), R.layout.appwidget);
            }

            // Hide the Loading indicator
            remoteViews.setViewVisibility(R.id.loading_indicator, View.GONE);

            // Always Refresh the Clock widget
            refreshClock(remoteViews);
            refreshAlarmStatus(remoteViews);

            WeatherInfo weatherInfo = Preferences.getCachedWeatherInfo(this);

            if (weatherInfo != null) {
                setWeatherData(remoteViews, weatherInfo);
            } else {
                setNoWeatherData(remoteViews);
            }

            // Weather Image
            //Drawable panelBackground = ThemeUtils.getDrawable(this, "weather_widget_background");
            //Bitmap panelBackgroundBitmap = ((BitmapDrawable)panelBackground).getBitmap();
            //remoteViews.setImageViewBitmap(R.id.weather_panel_background, panelBackgroundBitmap);

            float ratio = WidgetUtils.getScaleRatio(this, id);
            setClockSize(remoteViews, ratio);

            Context ctx = getBaseContext();

            boolean b24 = DateFormat.is24HourFormat(ctx);
            boolean isNormal = Preferences.weatherWidgetStyle(this).equals("normal");
            boolean isTransparent = Preferences.weatherWidgetStyle(this).equals("transparent");
            boolean isPanel = Preferences.weatherWidgetStyle(this).equals("panel");
            int resIdHour, resIdMinute;

            int mBigClockColor = isTransparent ? ThemeUtils.getColor(ctx, "big_clock_white") : ThemeUtils.getColor(ctx, "big_clock_black");
            int mWidgetDetailsColor = ThemeUtils.getColor(ctx, "widget_details");

            if (isTransparent || isPanel) {
                resIdMinute = R.string.widget_format_m_join;
            } else {
                resIdMinute = R.string.widget_format_m;
            }

            if (b24) {
                resIdHour = R.string.widget_24_hours_format_h;
            } else {
                if (isTransparent || isPanel) {
                    resIdHour = R.string.widget_12_hours_format_h_join;
                } else {
                    resIdHour = R.string.widget_12_hours_format_h;
                }
            }
            String formatHour = ctx.getString(resIdHour);
            String formatMinute = ctx.getString(resIdMinute);

            Date updateTime = new Date();
            String hourString = DateFormat.format(formatHour, updateTime).toString();
            String minuteString = DateFormat.format(formatMinute, updateTime).toString();

            remoteViews.setTextColor(R.id.clock1_bold, mBigClockColor);
            remoteViews.setTextViewText(R.id.clock1_bold, hourString);
            remoteViews.setTextColor(R.id.clock2_bold, mBigClockColor);
            remoteViews.setTextViewText(R.id.clock2_bold, minuteString);

            // Weather clock tab drawable
            if (isNormal) {
                Drawable tabBackground = ThemeUtils.getDrawable(this, "weather_clock_tab");
                Bitmap tabBitmap = ((BitmapDrawable)tabBackground).getBitmap();
                remoteViews.setImageViewBitmap(R.id.clock1_bold_background, tabBitmap);
                remoteViews.setImageViewBitmap(R.id.clock2_bold_background, tabBitmap);
            }

            String formatDate = ctx.getString(R.string.abbrev_wday_month_day_no_year);
            String dateString = DateFormat.format(formatDate, updateTime).toString();

            remoteViews.setTextColor(R.id.date_regular, mWidgetDetailsColor);
            remoteViews.setTextViewText(R.id.date_regular, dateString);

            // Do the update
            mAppWidgetManager.updateAppWidget(id, remoteViews);
        }
    }

    //===============================================================================================
    // Clock related functionality
    //===============================================================================================
    private void refreshClock(RemoteViews clockViews) {

        // Date/Alarm is common to both clocks, set it's size
        refreshDateAlarmFont(clockViews);

        // Register an onClickListener on Clock, starting DeskClock
        Intent i = WidgetUtils.getDefaultClockIntent(this);
        if (i != null) {
            PendingIntent pi = PendingIntent.getActivity(this, 0, i, PendingIntent.FLAG_UPDATE_CURRENT);
            clockViews.setOnClickPendingIntent(R.id.clock_panel, pi);
        }
    }

    private void refreshDateAlarmFont(RemoteViews clockViews) {
        // Show the panel
        clockViews.setViewVisibility(R.id.date_alarm, View.VISIBLE);
    }

    private void setClockSize(RemoteViews clockViews, float scale) {
        float fontSize = getResources().getDimension(R.dimen.widget_big_font_size);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            clockViews.setTextViewTextSize(R.id.clock1_bold, TypedValue.COMPLEX_UNIT_PX, fontSize * scale);
            clockViews.setTextViewTextSize(R.id.clock2_bold, TypedValue.COMPLEX_UNIT_PX, fontSize * scale);
        }
    }

    //===============================================================================================
    // Alarm related functionality
    //===============================================================================================
    private void refreshAlarmStatus(RemoteViews alarmViews) {
        String nextAlarm = getNextAlarm();
        if (!TextUtils.isEmpty(nextAlarm)) {
            // An alarm is set, deal with displaying it

            // set the imageview
            Drawable alarmResource = ThemeUtils.getDrawable(this, "ic_alarm_small");
            Bitmap alarmBitmap = ((BitmapDrawable)alarmResource).getBitmap();
            alarmViews.setImageViewBitmap(R.id.alarm_icon, alarmBitmap);
            alarmViews.setViewVisibility(R.id.alarm_icon, View.VISIBLE);
            alarmViews.setViewVisibility(R.id.nextAlarm_regular, View.VISIBLE);

            int mWidgetDetailsColor = ThemeUtils.getColor(this, "widget_details");
            alarmViews.setTextColor(R.id.nextAlarm_regular, mWidgetDetailsColor);
            alarmViews.setTextViewText(R.id.nextAlarm_regular, nextAlarm.toString().toUpperCase(Locale.getDefault()));
            return;
        }

        // No alarm set or Alarm display is hidden, hide the views
        alarmViews.setViewVisibility(R.id.alarm_icon, View.GONE);
        alarmViews.setViewVisibility(R.id.nextAlarm_regular, View.GONE);
    }

    /**
     * @return A formatted string of the next alarm or null if there is no next alarm.
     */
    private String getNextAlarm() {
        String nextAlarm = Settings.System.getString(
                getContentResolver(), Settings.System.NEXT_ALARM_FORMATTED);
        if (nextAlarm == null || TextUtils.isEmpty(nextAlarm)) {
            return null;
        }
        return nextAlarm;
    }

    //===============================================================================================
    // Weather related functionality
    //===============================================================================================
    /**
     * Display the weather information
     */
    private void setWeatherData(RemoteViews weatherViews, WeatherInfo w) {

        int mWidgetDetailsColor = ThemeUtils.getColor(this, "widget_details");

        // Weather Image
        Drawable conditionResource = ThemeUtils.getDrawable(this, w.getConditionResource());
        Bitmap conditionBitmap = ((BitmapDrawable)conditionResource).getBitmap();
        weatherViews.setImageViewBitmap(R.id.weather_image, conditionBitmap);

        // Weather Condition
        weatherViews.setTextColor(R.id.weather_condition, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_condition, w.getCondition());
        weatherViews.setViewVisibility(R.id.weather_condition, View.VISIBLE);

        // Weather Temps Panel
        weatherViews.setTextColor(R.id.weather_temp, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_temp, w.getFormattedTemperature());
        weatherViews.setViewVisibility(R.id.weather_temps_panel, View.VISIBLE);

        // Display the full weather information panel items
        // Load the preferences
        boolean showLocation = Preferences.showWeatherLocation(this);

        // City
        weatherViews.setTextColor(R.id.weather_city, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_city, w.getCity());
        weatherViews.setViewVisibility(R.id.weather_city, showLocation ? View.VISIBLE : View.GONE);

        // Weather Temps Panel additional items
        boolean invertLowhigh = Preferences.invertLowHighTemperature(this);
        final String low = w.getFormattedLow();
        final String high = w.getFormattedHigh();
        weatherViews.setTextColor(R.id.weather_low_high, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_low_high, invertLowhigh ? high + " | " + low : low + " | " + high);

        // Register an onClickListener on Weather
        setWeatherClickListener(weatherViews);
    }

    /**
     * There is no data to display, display 'empty' fields and the 'Tap to reload' message
     */
    private void setNoWeatherData(RemoteViews weatherViews) {
        final Resources res = getBaseContext().getResources();

        int mWidgetDetailsColor = ThemeUtils.getColor(this, "widget_details");

        // Weather Image - Either the default or alternate set
        Drawable conditionResource = ThemeUtils.getDrawable(this, "weather_unknown");
        Bitmap conditionBitmap = ((BitmapDrawable) conditionResource).getBitmap();
        weatherViews.setImageViewBitmap(R.id.weather_image, conditionBitmap);

        weatherViews.setTextColor(R.id.weather_city, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_city, res.getString(R.string.weather_no_data));
        weatherViews.setViewVisibility(R.id.weather_city, View.VISIBLE);

        weatherViews.setViewVisibility(R.id.weather_temps_panel, View.GONE);
        weatherViews.setTextColor(R.id.weather_condition, mWidgetDetailsColor);
        weatherViews.setTextViewText(R.id.weather_condition, res.getString(R.string.weather_tap_to_refresh));

        // Register an onClickListener on Weather
        setWeatherClickListener(weatherViews);
    }

    private void setWeatherClickListener(RemoteViews weatherViews) {
        //weatherViews.setOnClickPendingIntent(R.id.weather_panel,
                //WeatherUpdateService.getUpdateIntent(this, true));
        weatherViews.setOnClickPendingIntent(R.id.weather_panel,
                PendingIntent.getActivity(this, 0,
                    new Intent(this, com.lennox.weather.weather.WeatherForecastDialog.class), PendingIntent.FLAG_UPDATE_CURRENT));
    }

}
