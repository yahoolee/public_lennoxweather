/*
 * Copyright (C) 2012 The CyanogenMod Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather.misc;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;

import com.lennox.weather.weather.WeatherInfo;
import com.lennox.weather.weather.WeatherForecast;

import java.util.ArrayList;
import java.util.Set;

public class Preferences {
    private Preferences() {
    }

    public static boolean showWeatherLocation(Context context) {
        return getPrefs(context).getBoolean(Constants.WEATHER_SHOW_LOCATION, true);
    }

    public static boolean invertLowHighTemperature(Context context) {
        return getPrefs(context).getBoolean(Constants.WEATHER_INVERT_LOWHIGH, false);
    }

    public static boolean useMetricUnits(Context context) {
        return getPrefs(context).getBoolean(Constants.WEATHER_USE_METRIC, true);
    }

    public static long weatherRefreshIntervalInMs(Context context) {
        String value = getPrefs(context).getString(Constants.WEATHER_REFRESH_INTERVAL, "60");
        return Long.parseLong(value) * 60 * 1000;
    }

    public static String weatherWidgetStyle(Context context) {
        return getPrefs(context).getString(Constants.WEATHER_WIDGET_STYLE, "normal");
    }

    public static boolean useCustomWeatherLocation(Context context) {
        return getPrefs(context).getBoolean(Constants.WEATHER_USE_CUSTOM_LOCATION, false);
    }

    public static String customWeatherLocation(Context context) {
        return getPrefs(context).getString(Constants.WEATHER_CUSTOM_LOCATION_STRING, null);
    }

    public static void setCachedWeatherInfo(Context context, long timestamp, WeatherInfo data) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putLong(Constants.WEATHER_LAST_UPDATE, timestamp);
        if (data != null) {
            editor.putString(Constants.WEATHER_DATA, data.toSerializedString());
        }
        editor.apply();
    }

    public static void setCachedWeatherForecast(Context context, long timestamp, ArrayList<WeatherForecast> data) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        if (data != null) {
            editor.putString(Constants.WEATHER_FORECAST_0, data.get(0).toSerializedString());
            editor.putString(Constants.WEATHER_FORECAST_1, data.get(1).toSerializedString());
            editor.putString(Constants.WEATHER_FORECAST_2, data.get(2).toSerializedString());
            editor.putString(Constants.WEATHER_FORECAST_3, data.get(3).toSerializedString());
        }
        editor.apply();
    }

    public static long lastWeatherUpdateTimestamp(Context context) {
        return getPrefs(context).getLong(Constants.WEATHER_LAST_UPDATE, 0);
    }

    public static WeatherInfo getCachedWeatherInfo(Context context) {
        return WeatherInfo.fromSerializedString(context,
                getPrefs(context).getString(Constants.WEATHER_DATA, null));
    }

    public static ArrayList<WeatherForecast> getCachedWeatherForecast(Context context) {
        ArrayList<WeatherForecast> forecastArray = new ArrayList<WeatherForecast>();

        String[] prefKeys = {Constants.WEATHER_FORECAST_0, Constants.WEATHER_FORECAST_1,
                             Constants.WEATHER_FORECAST_2, Constants.WEATHER_FORECAST_3};

        for (String prefkey : prefKeys) {
            String result = getPrefs(context).getString(prefkey, null);
            if ( result != null && !result.equals("")) {
                forecastArray.add(WeatherForecast.fromSerializedString(context,result));
            }
        }

        return forecastArray;
    }

    public static String getCachedWoeid(Context context) {
        return getPrefs(context).getString(Constants.WEATHER_WOEID, null);
    }

    public static void setCachedWoeid(Context context, String woeid) {
        getPrefs(context).edit().putString(Constants.WEATHER_WOEID, woeid).apply();
    }

    public static String getCachedLocationUrl(Context context) {
        return getPrefs(context).getString(Constants.WEATHER_LOCATION_URL, null);
    }

    public static void setCachedLocationUrl(Context context, String url) {
        getPrefs(context).edit().putString(Constants.WEATHER_LOCATION_URL, url).apply();
    }

    public static final int SHOW_NEVER = 0;
    public static final int SHOW_FIRST_LINE = 1;
    public static final int SHOW_ALWAYS = 2;

    public static SharedPreferences getPrefs(Context context) {
        return context.getSharedPreferences(Constants.PREF_NAME, Context.MODE_PRIVATE);
    }
}
