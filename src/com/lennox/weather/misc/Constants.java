/*
 * Copyright (C) 2012 The CyanogenMod Project (DvTonder)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather.misc;

import android.text.format.DateUtils;

public class Constants {
    public static final boolean DEBUG = false;

    public static final String PREF_NAME = "LennoxWeather";

    public static final String WEATHER_USE_CUSTOM_LOCATION = "weather_use_custom_location";
    public static final String WEATHER_CUSTOM_LOCATION_STRING = "weather_custom_location_string";
    public static final String WEATHER_SHOW_LOCATION = "weather_show_location";
    public static final String WEATHER_SHOW_TIMESTAMP = "weather_show_timestamp";
    public static final String WEATHER_USE_METRIC = "weather_use_metric";
    public static final String WEATHER_INVERT_LOWHIGH = "weather_invert_lowhigh";
    public static final String WEATHER_REFRESH_INTERVAL = "weather_refresh_interval";
    public static final String WEATHER_USE_ALTERNATE_ICONS = "weather_use_alternate_icons";
    public static final String WEATHER_WOEID = "weather_woeid";
    public static final String WEATHER_LOCATION_URL = "weather_location_url";
    public static final String WEATHER_MODE = "weather_mode";
    public static final String WEATHER_WIDGET_STYLE = "weather_widget_style";
    public static final String WEATHER_VERSION = "preferences_application_version";

    // other shared pref entries
    public static final String WEATHER_LAST_UPDATE = "last_weather_update";
    public static final String WEATHER_DATA = "weather_data";
    public static final String WEATHER_FORECAST_0 = "weather_forecast_0";
    public static final String WEATHER_FORECAST_1 = "weather_forecast_1";
    public static final String WEATHER_FORECAST_2 = "weather_forecast_2";
    public static final String WEATHER_FORECAST_3 = "weather_forecast_3";

}
