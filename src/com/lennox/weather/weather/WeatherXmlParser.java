/******************************************************************************
 * Class       : YahooWeatherHelper.java                                                                  *
 * Parser helper for Yahoo                                                    *
 *                                                                            *
 * Version     : v1.0                                                         *
 * Date        : May 06, 2011                                                 *
 * Copyright (c)-2011 DatNQ some right reserved                               *
 * You can distribute, modify or what ever you want but WITHOUT ANY WARRANTY  *
 * Be honest by keep credit of this file                                      *
 *                                                                            *
 * If you have any concern, feel free to contact with me via email, i will    *
 * check email in free time                                                   * 
 * Email: nguyendatnq@gmail.com                                               *
 * ---------------------------------------------------------------------------*
 * Modification Logs:                                                         *
 *   KEYCHANGE  DATE          AUTHOR   DESCRIPTION                            *
 * ---------------------------------------------------------------------------*
 *    -------   May 06, 2011  DatNQ    Create new                             *
 ******************************************************************************/
/*
 * Modification into Android-internal WeatherXmlParser.java
 * Copyright (C) 2012 The AOKP Project
 */

package com.lennox.weather.weather;

import com.lennox.weather.misc.Preferences;

import java.io.StringReader;

import java.util.ArrayList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.util.Log;

public class WeatherXmlParser {

    protected static final String TAG = "WeatherXmlParser";

    /** Yahoo attributes */
    private static final String PARAM_YAHOO_LOCATION = "yweather:location";
    private static final String PARAM_YAHOO_UNIT = "yweather:units";
    private static final String PARAM_YAHOO_ASTRONOMY = "yweather:astronomy";
    private static final String PARAM_YAHOO_ATMOSPHERE = "yweather:atmosphere";
    private static final String PARAM_YAHOO_CONDITION = "yweather:condition";
    private static final String PARAM_YAHOO_WIND = "yweather:wind";
    private static final String PARAM_YAHOO_FORECAST = "yweather:forecast";
    private static final String PARAM_YAHOO_LINK = "link";

    private static final String ATT_YAHOO_CITY = "city";
    private static final String ATT_YAHOO_TEMP = "temp";
    private static final String ATT_YAHOO_CODE = "code";
    private static final String ATT_YAHOO_TEMP_UNIT = "temperature";
    private static final String ATT_YAHOO_DISTANCE_UNIT = "distance";
    private static final String ATT_YAHOO_HUMIDITY = "humidity";
    private static final String ATT_YAHOO_VISIBILITY = "visibility";
    private static final String ATT_YAHOO_PRESSURE = "pressure";
    private static final String ATT_YAHOO_RISING = "rising";
    private static final String ATT_YAHOO_TEXT = "text";
    private static final String ATT_YAHOO_DATE = "date";
    private static final String ATT_YAHOO_DAY = "day";
    private static final String ATT_YAHOO_CHILL = "chill";
    private static final String ATT_YAHOO_SPEED = "speed";
    private static final String ATT_YAHOO_DIRECTION = "direction";
    private static final String ATT_YAHOO_TODAY_HIGH = "high";
    private static final String ATT_YAHOO_TODAY_LOW = "low";
    private static final String ATT_YAHOO_SUNRISE = "sunrise";
    private static final String ATT_YAHOO_SUNSET = "sunset";

    private Context mContext;

    public WeatherXmlParser(Context context) {
        mContext = context;
    }

    private String getString(String tagName, Element element) {
        NodeList list = element.getElementsByTagName(tagName);
        if (list != null && list.getLength() > 0) {
            NodeList subList = list.item(0).getChildNodes();
            if (subList != null && subList.getLength() > 0) {
                return subList.item(0).getNodeValue();
            }
        }
        return null;
    }

    private String getValueForAttribute(Element root, String tagName, String attributeName, int number) {
        NamedNodeMap node = root.getElementsByTagName(tagName).item(number).getAttributes();
        if (node == null) {
            return null;
        }
        return node.getNamedItem(attributeName).getNodeValue();
    }

    private float getFloatForAttribute(Element root, String tagName, String attributeName, int number)
            throws NumberFormatException {
        String value = getValueForAttribute(root, tagName, attributeName, number);
        if (value == null || value.equals("")) {
            return Float.NaN;
        }
        return Float.parseFloat(value);
    }

    private int getIntForAttribute(Element root, String tagName, String attributeName, int number)
            throws NumberFormatException {
        String value = getValueForAttribute(root, tagName, attributeName, number);
        if (value == null || value.equals("")) {
            return -1;
        }
        return Integer.parseInt(value);
    }

    public WeatherInfo parseWeatherResponse(Document docWeather) {
        if (docWeather == null) {
            Log.e(TAG, "Invalid doc weather");
            return null;
        }

        try {
            Element root = docWeather.getDocumentElement();
            root.normalize();

            WeatherInfo w = new WeatherInfo(mContext,
                    /* city */ getValueForAttribute(root, PARAM_YAHOO_LOCATION, ATT_YAHOO_CITY, 0),
                    /* forecastDate */ getValueForAttribute(root, PARAM_YAHOO_CONDITION, ATT_YAHOO_DATE, 0),
                    /* condition */ getValueForAttribute(root, PARAM_YAHOO_CONDITION, ATT_YAHOO_TEXT, 0),
                    /* conditionCode */ getIntForAttribute(root, PARAM_YAHOO_CONDITION, ATT_YAHOO_CODE, 0),
                    /* temperature */ getFloatForAttribute(root, PARAM_YAHOO_CONDITION, ATT_YAHOO_TEMP, 0),
                    /* low */ getFloatForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_TODAY_LOW, 0),
                    /* high */ getFloatForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_TODAY_HIGH, 0),
                    /* tempUnit */ getValueForAttribute(root, PARAM_YAHOO_UNIT, ATT_YAHOO_TEMP_UNIT, 0),
                    /* humidity */ getFloatForAttribute(root, PARAM_YAHOO_ATMOSPHERE, ATT_YAHOO_HUMIDITY, 0),
                    /* visibility */ getFloatForAttribute(root, PARAM_YAHOO_ATMOSPHERE, ATT_YAHOO_VISIBILITY, 0),
                    /* distanceUnit */ getValueForAttribute(root, PARAM_YAHOO_UNIT, ATT_YAHOO_DISTANCE_UNIT, 0),
                    /* pressure */ getFloatForAttribute(root, PARAM_YAHOO_ATMOSPHERE, ATT_YAHOO_PRESSURE, 0),
                    /* pressureUnit */ getValueForAttribute(root, PARAM_YAHOO_UNIT, ATT_YAHOO_PRESSURE, 0),
                    /* rising */ getIntForAttribute(root, PARAM_YAHOO_ATMOSPHERE, ATT_YAHOO_RISING, 0),
                    /* sunrise */ getValueForAttribute(root, PARAM_YAHOO_ASTRONOMY, ATT_YAHOO_SUNRISE, 0),
                    /* sunset */ getValueForAttribute(root, PARAM_YAHOO_ASTRONOMY, ATT_YAHOO_SUNSET, 0),
                    /* wind */ getFloatForAttribute(root, PARAM_YAHOO_WIND, ATT_YAHOO_SPEED, 0),
                    /* windChill */ getIntForAttribute(root, PARAM_YAHOO_WIND, ATT_YAHOO_CHILL, 0),
                    /* windDir */ getIntForAttribute(root, PARAM_YAHOO_WIND, ATT_YAHOO_DIRECTION, 0),
                    /* speedUnit */ getValueForAttribute(root, PARAM_YAHOO_UNIT, ATT_YAHOO_SPEED, 0),
                    System.currentTimeMillis());

            Preferences.setCachedLocationUrl(mContext, getString(PARAM_YAHOO_LINK, root));

            Log.d(TAG, "Weather updated: " + w);
            return w;
        } catch (Exception e) {
            Log.e(TAG, "Couldn't parse Yahoo weather XML", e);
            return null;
        }
    }

    public ArrayList<WeatherForecast> parseWeatherResponseForecast(Document docWeather) {
        if (docWeather == null) {
            Log.e(TAG, "Invalid doc weather");
            return null;
        }

        try {
            Element root = docWeather.getDocumentElement();
            root.normalize();

            ArrayList<WeatherForecast> forecastArray = new ArrayList<WeatherForecast>();

            for (int i = 1; i < 5; i++) {
                WeatherForecast w = new WeatherForecast(mContext,
                    /* day */ getValueForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_DAY, i),
                    /* date */ getValueForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_DATE, i),
                    /* condition */ getValueForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_TEXT, i),
                    /* conditionCode */ getIntForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_CODE, i),
                    /* low */ getIntForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_TODAY_LOW, i),
                    /* high */ getIntForAttribute(root, PARAM_YAHOO_FORECAST, ATT_YAHOO_TODAY_HIGH, i));

                Log.d(TAG, "Weather updated: " + w.toString());
                forecastArray.add(w);
            }
            return forecastArray;
        } catch (Exception e) {
            Log.e(TAG, "Couldn't parse Yahoo weather XML", e);
            return null;
        }
    }

    public String parsePlaceFinderResponse(String response) {
        try {
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            Document doc = db.parse(new InputSource(new StringReader(response)));

            NodeList resultNodes = doc.getElementsByTagName("Result");

            Node resultNode = resultNodes.item(0);
            NodeList attrsList = resultNode.getChildNodes();

            for (int i = 0; i < attrsList.getLength(); i++) {
                Node node = attrsList.item(i);
                Node firstChild = node.getFirstChild();
                if ("woeid".equalsIgnoreCase(node.getNodeName()) && firstChild != null) {
                    return firstChild.getNodeValue();
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "Couldn't parse Yahoo place finder XML", e);
        }
        return null;
    }
}
