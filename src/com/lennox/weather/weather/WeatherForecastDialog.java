package com.lennox.weather.weather;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
 
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.lennox.weather.misc.Preferences;
import com.lennox.weather.utils.ThemeUtils;
import com.lennox.weather.weather.WeatherForecast;
import com.lennox.weather.weather.WeatherInfo;
import com.lennox.weather.weather.WeatherUpdateService;
import com.lennox.weather.R;

public class WeatherForecastDialog extends Activity {
 
    ListView forecastList;
    public static Context mContext;
    PendingIntent pendingIntent;
    ArrayList<WeatherForecast> forecastArray;
    WeatherInfo currentWeatherInfo;

    private TextView currentText;
    private TextView lowHighText;
    private TextView conditionText;
    private TextView locationText;
    private TextView dateText;
    private TextView dayText;

    private TextView humidityText;
    private TextView windSpeedText;
    //private TextView windChillText;
    private TextView visibilityText;
    private TextView pressureText;
    private TextView sunriseText;
    private TextView sunsetText;

    private final OnClickListener mPreferencesClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, com.lennox.weather.preference.WeatherPreferences.class);
  	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  	    mContext.startActivity(intent);
            refreshView();
        }
    };

    private final OnClickListener mRefreshClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            refreshView();
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pendingIntent = WeatherUpdateService.getUpdateIntent(this, true);
	mContext = getApplicationContext();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onPause(){
    	super.onPause();
    }

    @Override
    public void onResume() {
        initialiseView();
        refreshView();
        super.onResume();
    }

    public void initialiseView() {
        setContentView(R.layout.forecast_dialog);

        findViewById(R.id.preferences).setOnClickListener(mPreferencesClickListener);
        findViewById(R.id.refresh).setOnClickListener(mRefreshClickListener);

        currentText = (TextView)findViewById(R.id.current_temperature);
        lowHighText = (TextView)findViewById(R.id.minimum_maximum_temperature);
        conditionText = (TextView)findViewById(R.id.current_condition);
        locationText = (TextView)findViewById(R.id.current_location);
        dateText = (TextView)findViewById(R.id.current_date);
        dayText = (TextView)findViewById(R.id.current_day);

        humidityText = (TextView)findViewById(R.id.current_humidity);
        windSpeedText = (TextView)findViewById(R.id.current_wind_speed);
        //windChillText = (TextView)findViewById(R.id.current_wind_chill);
        visibilityText = (TextView)findViewById(R.id.current_visibility);
        pressureText = (TextView)findViewById(R.id.current_pressure);
        sunriseText = (TextView)findViewById(R.id.sunrise);
        sunsetText = (TextView)findViewById(R.id.sunset);

    }

    public void refreshView() {
        forecastArray = Preferences.getCachedWeatherForecast(this);
        currentWeatherInfo = Preferences.getCachedWeatherInfo(this);
        if ( forecastArray == null || forecastArray.size() < 1 || currentWeatherInfo == null ) {
            Intent intent = new Intent();  
            try {  
                pendingIntent.send(this, 0, intent);  
            } catch (PendingIntent.CanceledException e) {
            }
            intent = new Intent(mContext, com.lennox.weather.preference.WeatherPreferences.class);
  	    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  	    mContext.startActivity(intent);
            finish();
        } else {
            Intent intent = new Intent();  
            try {  
                pendingIntent.send(mContext, 0, intent);  
            } catch (PendingIntent.CanceledException e) {
            }
            setupInfoView();
            setupForecastView();
        }
    }

    private void setupInfoView() {
        SimpleDateFormat sdf = new SimpleDateFormat("EEEE");
        Date d = new Date();
        String dayOfTheWeek = sdf.format(d);
        sdf = new SimpleDateFormat("dd MMMM");
        String currentDate = sdf.format(d);

        String lowHigh = currentWeatherInfo.getFormattedLow() + " | " + currentWeatherInfo.getFormattedHigh();
        if (Preferences.invertLowHighTemperature(mContext)) {
            lowHigh = currentWeatherInfo.getFormattedHigh() + " | " + currentWeatherInfo.getFormattedLow();
        }

        currentText.setText(currentWeatherInfo.getFormattedTemperature());
        lowHighText.setText(lowHigh);
        conditionText.setText(currentWeatherInfo.getCondition());
        locationText.setText(currentWeatherInfo.getCity());
        dateText.setText(currentDate);
        dayText.setText(dayOfTheWeek);

        humidityText.setText(mContext.getString(R.string.humidity) +
                             ": " + currentWeatherInfo.getFormattedHumidity());
        windSpeedText.setText(mContext.getString(R.string.wind) +
                             ": " + currentWeatherInfo.getFormattedWindSpeed() +
                             " " + currentWeatherInfo.getWindDirection());
        //windChillText.setText(currentWeatherInfo.getWindChill());
        visibilityText.setText(mContext.getString(R.string.visibility) +
                             ": " + currentWeatherInfo.getFormattedVisibility());
        pressureText.setText(mContext.getString(R.string.pressure) +
                             ": " + currentWeatherInfo.getFormattedPressure() + " (" +
                             currentWeatherInfo.getPressureRisingCharacter() + ")");
        sunriseText.setText(mContext.getString(R.string.sunrise) +
                             ": " + currentWeatherInfo.getSunrise());
        sunsetText.setText(mContext.getString(R.string.sunset) +
                             ": " + currentWeatherInfo.getSunset());

        ImageView img = (ImageView)findViewById(R.id.yahoo_logo);
        img.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.addCategory(Intent.CATEGORY_BROWSABLE);
                intent.setData(Uri.parse(Preferences.getCachedLocationUrl(mContext)));
                startActivity(intent);
            }
        });

        ImageView backgroundImage = (ImageView) findViewById(R.id.image_background);
        backgroundImage.setImageDrawable(ThemeUtils.getDrawable(mContext,
                                         currentWeatherInfo.getConditionBackground()));
    }

    private void setupForecastView() {
        int[] layouts = { R.id.Day1Weather, R.id.Day2Weather, R.id.Day3Weather, R.id.Day4Weather };
        for (int i = 0; i < forecastArray.size(); i++) {
            View v = findViewById(layouts[i]);
            WeatherForecast currentForecast = forecastArray.get(i);
            TextView lowText = (TextView)v.findViewById(R.id.weather_low);
            TextView highText = (TextView)v.findViewById(R.id.weather_high);
            TextView conditionText = (TextView)v.findViewById(R.id.weather_condition);
            TextView dayText = (TextView)v.findViewById(R.id.weather_day);
            if (Preferences.invertLowHighTemperature(mContext)) {
                lowText.setText(currentForecast.getFormattedHigh());
                lowText.setSelected(true);
                highText.setText(currentForecast.getFormattedLow());
                highText.setSelected(true);
            } else {
                lowText.setText(currentForecast.getFormattedLow());
                lowText.setSelected(true);
                highText.setText(currentForecast.getFormattedHigh());
                highText.setSelected(true);
            }
            conditionText.setText(currentForecast.getCondition());
            conditionText.setSelected(true);
            dayText.setText(currentForecast.getDay());
            dayText.setSelected(true);
            ImageView iv = (ImageView)v.findViewById(R.id.weather_image);
            iv.setImageDrawable(ThemeUtils.getDrawable(mContext,
                                         currentForecast.getConditionResource()));

        }
    }

}
