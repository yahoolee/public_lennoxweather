/*
 * Copyright (C) 2012 The AOKP Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather.weather;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.lennox.weather.R;
import com.lennox.weather.misc.Preferences;
import com.lennox.weather.misc.WidgetUtils;

import java.text.DecimalFormat;
import java.util.*;

public class WeatherInfo {
    private static final DecimalFormat sNoDigitsFormat = new DecimalFormat("0");

    private Context mContext;

    private String city;
    private String forecastDate;
    private String condition;
    private int conditionCode;
    private float temperature;
    private float lowTemperature;
    private float highTemperature;
    private String tempUnit;
    private float humidity;
    private float visibility;
    private String distanceUnit;
    private float pressure;
    private String pressureUnit;
    private int rising;
    private String sunrise;
    private String sunset;
    private float wind;
    private int windChill;
    private int windDirection;
    private String speedUnit;
    private long timestamp;

    final static int[] stormCodes = { 0, 3, 4, 17, 35, 37, 38, 39 };
    final static int[] rainCodes = { 1, 2, 9, 10, 11, 12, 40, 45, 47 };
    final static int[] coldCodes = { 5, 6 };
    final static int[] snowCodes = { 7, 13, 14, 15, 16, 18, 41, 42, 43 };
    final static int[] cloudyCodes = { 8, 19, 25, 26 };
    final static int[] fogCodes = { 20, 21, 22 };
    final static int[] windyCodes = { 23, 24 };
    final static int[] partlyCloudyCodes = { 27, 28, 29, 30, 44 };
    final static int[] sunnyCodes = { 31, 32, 33, 34 };
    final static int[] hotCodes = { 36 };
    final static int[] showerCodes = { 46 };

    final static String[] backgroundValues
        = { "bg_thunderstorms", "bg_rain", "bg_rain", "bg_thunderstorms", "bg_thunderstorms",
            "bg_cold", "bg_cold", "bg_snow", "bg_cloudy", "bg_rain",
            "bg_rain", "bg_rain", "bg_rain", "bg_snow", "bg_snow",
            "bg_snow", "bg_snow", "bg_thunderstorms", "bg_snow", "bg_cloudy",
            "bg_fog", "bg_fog", "bg_fog", "bg_windy", "bg_windy",
            "bg_cloudy", "bg_cloudy", "bg_mostly_cloudy", "bg_mostly_cloudy", "bg_mostly_cloudy",
            "bg_mostly_cloudy", "bg_sunny", "bg_sunny", "bg_sunny", "bg_sunny",
            "bg_thunderstorms", "bg_hot", "bg_thunderstorms", "bg_thunderstorms", "bg_thunderstorms",
            "bg_rain", "bg_snow", "bg_snow", "bg_snow", "bg_mostly_cloudy",
            "bg_rain", "bg_shower", "bg_rain" };

    final static String[] iconValues
        = { "weather_thunderstorms", "weather_rain", "weather_rain", "weather_thunderstorms", "weather_thunderstorms",
            "weather_cold", "weather_cold", "weather_snow", "weather_cloudy", "weather_rain",
            "weather_rain", "weather_rain", "weather_rain", "weather_snow", "weather_snow",
            "weather_snow", "weather_snow", "weather_thunderstorms", "weather_snow", "weather_cloudy",
            "weather_fog", "weather_fog", "weather_fog", "weather_windy", "weather_windy",
            "weather_cloudy", "weather_cloudy", "weather_mostly_cloudy", "weather_mostly_cloudy", "weather_mostly_cloudy",
            "weather_mostly_cloudy", "weather_sunny", "weather_sunny", "weather_sunny", "weather_sunny",
            "weather_thunderstorms", "weather_hot", "weather_thunderstorms", "weather_thunderstorms", "weather_thunderstorms",
            "weather_rain", "weather_snow", "weather_snow", "weather_snow", "weather_mostly_cloudy",
            "weather_rain", "weather_shower", "weather_rain" };

    final static String[] pressureRisingValues
        = { "-", "\u2191", "\u2193" };

    public WeatherInfo(Context context,
            String city, String fdate, String condition, int conditionCode,
            float temp, float low, float high, String tempUnit, float humidity,
            float visibility, String distanceUnit, float pressure, String pressureUnit,
            int rising, String sunrise, String sunset, float wind, int windChill,
            int windDir, String speedUnit, long timestamp) {

        this.mContext = context;
        this.city = city;
        this.forecastDate = fdate;
        this.condition = condition;
        this.conditionCode = conditionCode;
        this.temperature = temp;
        this.lowTemperature = low;
        this.highTemperature = high;
        this.tempUnit = tempUnit;
        this.humidity = humidity;
        this.visibility = visibility;
        this.distanceUnit = distanceUnit;
        this.pressure = pressure;
        this.pressureUnit = pressureUnit;
        this.rising = rising;
        this.sunrise = sunrise;
        this.sunset = sunset;
        this.wind = wind;
        this.windChill = windChill;
        this.windDirection = windDir;
        this.speedUnit = speedUnit;
        this.timestamp = timestamp;
    }

    public String getConditionResource() {
        if (conditionCode < 0 || conditionCode > 47 ) {
            return "weather_unknown";
        } else {
            return iconValues[conditionCode];
        }
    }

    public String getConditionBackground() {
        if (conditionCode < 0 || conditionCode > 47 ) {
            return "bg_sunny";
        } else {
            return backgroundValues[conditionCode];
        }
    }

    public String getPressureRisingCharacter() {
        if (0 <= rising && rising <= 2) {
            return pressureRisingValues[rising];
        } else {
            return pressureRisingValues[0];
        }
    }

    public String getCity() {
        return city;
    }

    public String getCondition() {
        final Resources res = mContext.getResources();
        final int resId = res.getIdentifier("weather_" + conditionCode, "string", mContext.getPackageName());
        if (resId != 0) {
            return res.getString(resId);
        }
        return condition;
    }

    public Date getTimestamp() {
        return new Date(timestamp);
    }

    private String getFormattedValue(float value, String unit) {
        if (Float.isNaN(highTemperature)) {
            return "-";
        }
        return sNoDigitsFormat.format(value) + unit;
    }

    public String getFormattedTemperature() {
        return getFormattedValue(temperature, "°" + tempUnit);
    }

    public String getFormattedLow() {
        return getFormattedValue(lowTemperature, "°");
    }

    public String getFormattedHigh() {
        return getFormattedValue(highTemperature, "°");
    }

    public String getFormattedHumidity() {
        return getFormattedValue(humidity, "%");
    }

    public String getFormattedWindSpeed() {
        return getFormattedValue(wind, speedUnit);
    }

    public String getFormattedPressure() {
        return getFormattedValue(pressure, pressureUnit);
    }

    public String getFormattedVisibility() {
        return getFormattedValue(visibility, distanceUnit);
    }

    public String getSunrise() {
        return sunrise;
    }

    public String getSunset() {
        return sunset;
    }

    public String getWindChill() {
        return String.valueOf(windChill);
    }

    public String getWindDirection() {
        int resId;

        if (windDirection < 0) {
            return "";
        }

        if (windDirection < 23) resId = R.string.weather_N;
        else if (windDirection < 68) resId = R.string.weather_NE;
        else if (windDirection < 113) resId = R.string.weather_E;
        else if (windDirection < 158) resId = R.string.weather_SE;
        else if (windDirection < 203) resId = R.string.weather_S;
        else if (windDirection < 248) resId = R.string.weather_SW;
        else if (windDirection < 293) resId = R.string.weather_W;
        else if (windDirection < 338) resId = R.string.weather_NW;
        else resId = R.string.weather_N;

        return mContext.getString(resId);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WeatherInfo for ");
        builder.append(city);
        builder.append("@ ");
        builder.append(getTimestamp());
        builder.append(": ");
        builder.append(getCondition());
        builder.append("(");
        builder.append(conditionCode);
        builder.append("), temperature ");
        builder.append(getFormattedTemperature());
        builder.append(", low ");
        builder.append(getFormattedLow());
        builder.append(", high ");
        builder.append(getFormattedHigh());
        builder.append(", humidity ");
        builder.append(getFormattedHumidity());
        builder.append(", wind ");
        builder.append(getFormattedWindSpeed());
        builder.append(" at ");
        builder.append(getWindDirection());
        return builder.toString();
    }

    public String toSerializedString() {
        StringBuilder builder = new StringBuilder();
        builder.append(city).append('|');
        builder.append(forecastDate).append('|');
        builder.append(condition).append('|');
        builder.append(conditionCode).append('|');
        builder.append(temperature).append('|');
        builder.append(lowTemperature).append('|');
        builder.append(highTemperature).append('|');
        builder.append(tempUnit).append('|');
        builder.append(humidity).append('|');
        builder.append(visibility).append('|');
        builder.append(distanceUnit).append('|');
        builder.append(pressure).append('|');
        builder.append(pressureUnit).append('|');
        builder.append(rising).append('|');
        builder.append(sunrise).append('|');
        builder.append(sunset).append('|');
        builder.append(wind).append('|');
        builder.append(windChill).append('|');
        builder.append(windDirection).append('|');
        builder.append(speedUnit).append('|');
        builder.append(timestamp);
        return builder.toString();
    }

    public static WeatherInfo fromSerializedString(Context context, String input) {
        if (input == null) {
            return null;
        }

        String[] parts = input.split("\\|");
        if (parts == null || parts.length != 21) {
            return null;
        }

        int conditionCode, rising, windChill, windDirection;
        long timestamp;
        float temperature, low, high, humidity, visibility, pressure, wind;

        try {
            conditionCode = Integer.parseInt(parts[3]);
            temperature = Float.parseFloat(parts[4]);
            low = Float.parseFloat(parts[5]);
            high = Float.parseFloat(parts[6]);
            humidity = Float.parseFloat(parts[8]);
            visibility = Float.parseFloat(parts[9]);
            pressure = Float.parseFloat(parts[11]);
            rising = Integer.parseInt(parts[13]);
            wind = Float.parseFloat(parts[16]);
            windChill = Integer.parseInt(parts[17]);
            windDirection = Integer.parseInt(parts[18]);
            timestamp = Long.parseLong(parts[20]);
        } catch (NumberFormatException e) {
            return null;
        }

        return new WeatherInfo(context,
                parts[0], parts[1], parts[2], conditionCode, temperature, low, high,
                parts[7], humidity, visibility, parts[10], pressure, parts[12],
                rising, parts[14], parts[15], wind, windChill, windDirection, parts[19],
                timestamp);
    }
}