/*
 * Copyright (C) 2012 The AOKP Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.lennox.weather.weather;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;

import com.lennox.weather.R;
import com.lennox.weather.misc.WidgetUtils;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Date;

import com.lennox.weather.misc.Preferences;

public class WeatherForecast {
    private static final DecimalFormat sNoDigitsFormat = new DecimalFormat("0");

    private Context mContext;

    private String day;
    private String date;
    private String condition;
    private int conditionCode;
    private int low;
    private int high;

    public WeatherForecast(Context context,
            String day, String date, String condition, int conditionCode, int low, int high) {
        this.mContext = context;
        this.day = day;
        this.date = date;
        this.condition = condition;
        this.conditionCode = conditionCode;
        this.low = low;
        this.high = high;
    }

    public String getConditionResource() {
        if (conditionCode < 0 || conditionCode > 47 ) {
            return "weather_unknown";
        } else {
            return WeatherInfo.iconValues[conditionCode];
        }
    }

    public String getCondition() {
        final Resources res = mContext.getResources();
        final int resId = res.getIdentifier("weather_" + conditionCode, "string", mContext.getPackageName());
        if (resId != 0) {
            return res.getString(resId);
        }
        return condition;
    }

    public String getDay() {
        String[] dayNamesApi = new String[]{
                 "Mon",
                 "Tue",
                 "Wed",
                 "Thu",
                 "Fri",
                 "Sat",
                 "Sun" };

        int index = -1;

        for (int i = 0; i < dayNamesApi.length; i++) {
            if ( dayNamesApi[i].equals(day) ) {
                index = i;
                break;
            }
        }

        if ( index != -1 ) {
            Calendar c = Calendar.getInstance();
            // date doesn't matter - it has to be a Monday
            // I new that first August 2011 is one ;-)
            c.set(2011, 7, 1, 0, 0, 0);
            c.add(Calendar.DAY_OF_MONTH, index);
            return String.format("%tA", c);
        } else {
            return day;
        }

    }

    private String getFormattedValue(float value, String unit) {
        if (Float.isNaN(high)) {
            return "-";
        }
        return sNoDigitsFormat.format(value) + unit;
    }

    public String getFormattedLow() {
        return getFormattedValue(low, "°");
    }

    public String getFormattedHigh() {
        return getFormattedValue(high, "°");
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append("WeatherForecast for ");
        builder.append(day);
        builder.append(": ");
        builder.append(getCondition());
        builder.append("(");
        builder.append(conditionCode);
        builder.append("), low ");
        builder.append(getFormattedLow());
        builder.append(", high ");
        builder.append(getFormattedHigh());
        return builder.toString();
    }

    public String toSerializedString() {
        StringBuilder builder = new StringBuilder();
        builder.append(day).append('|');
        builder.append(date).append('|');
        builder.append(condition).append('|');
        builder.append(conditionCode).append('|');
        builder.append(low).append('|');
        builder.append(high);
        return builder.toString();
    }

    public static WeatherForecast fromSerializedString(Context context, String input) {
        if (input == null) {
            return null;
        }

        String[] parts = input.split("\\|");
        if (parts == null || parts.length != 6) {
            return null;
        }

        int conditionCode, low, high;

        try {
            conditionCode = Integer.parseInt(parts[3]);
            low = Integer.parseInt(parts[4]);
            high = Integer.parseInt(parts[5]);
        } catch (NumberFormatException e) {
            return null;
        }

        return new WeatherForecast(context,
                /* day */ parts[0], /* date */ parts[1], /* condition */ parts[2],
                conditionCode, low, high);
    }
}
